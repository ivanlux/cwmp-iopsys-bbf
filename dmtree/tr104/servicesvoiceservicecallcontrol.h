/*
 * Copyright (C) 2020 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	Author: Yalu Zhang, yalu.zhang@iopsys.eu
 */

#ifndef __SERVICESVOICESERVICECALLCONTROL_H
#define __SERVICESVOICESERVICECALLCONTROL_H

#include <libbbf_api/dmcommon.h>

extern DMOBJ tServicesVoiceServiceCallControlObj[];
extern DMLEAF tServicesVoiceServiceCallControlLineParams[];
extern DMLEAF tServicesVoiceServiceCallControlIncomingMapParams[];
extern DMLEAF tServicesVoiceServiceCallControlOutgoingMapParams[];
extern DMLEAF tServicesVoiceServiceCallControlNumberingPlanParams[];
extern DMOBJ tServicesVoiceServiceCallControlCallingFeaturesObj[];
extern DMOBJ tServicesVoiceServiceCallControlCallingFeaturesSetObj[];
extern DMLEAF tServicesVoiceServiceCallControlCallingFeaturesSetParams[];
extern DMLEAF tServicesVoiceServiceCallControlCallingFeaturesSetSCREJParams[];


#endif //__SERVICESVOICESERVICECALLCONTROL_H

