/*
 * Copyright (C) 2020 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	Author: Yalu Zhang, yalu.zhang@iopsys.eu
 */

#ifndef __SERVICESVOICESERVICESIP_H
#define __SERVICESVOICESERVICESIP_H

#include <libbbf_api/dmcommon.h>

extern DMOBJ tServicesVoiceServiceSIPObj[];
extern DMOBJ tServicesVoiceServiceSIPClientObj[];
extern DMLEAF tServicesVoiceServiceSIPClientParams[];
extern DMLEAF tServicesVoiceServiceSIPClientContactParams[];
extern DMOBJ tServicesVoiceServiceSIPNetworkObj[];
extern DMLEAF tServicesVoiceServiceSIPNetworkParams[];
extern DMLEAF tServicesVoiceServiceSIPNetworkFQDNServerParams[];


#endif //__SERVICESVOICESERVICESIP_H

