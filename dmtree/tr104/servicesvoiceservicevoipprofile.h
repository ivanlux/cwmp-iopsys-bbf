/*
 * Copyright (C) 2020 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	Author: Yalu Zhang, yalu.zhang@iopsys.eu
 */

#ifndef __SERVICESVOICESERVICEVOIPPROFILE_H
#define __SERVICESVOICESERVICEVOIPPROFILE_H

#include <libbbf_api/dmcommon.h>

extern DMOBJ tServicesVoiceServiceVoIPProfileObj[];
extern DMLEAF tServicesVoiceServiceVoIPProfileParams[];
extern DMOBJ tServicesVoiceServiceVoIPProfileRTPObj[];
extern DMLEAF tServicesVoiceServiceVoIPProfileRTPParams[];
extern DMLEAF tServicesVoiceServiceVoIPProfileRTPRTCPParams[];
extern DMLEAF tServicesVoiceServiceVoIPProfileRTPSRTPParams[];


#endif //__SERVICESVOICESERVICEVOIPPROFILE_H

