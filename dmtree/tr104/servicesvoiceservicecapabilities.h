/*
 * Copyright (C) 2020 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	Author: Yalu Zhang, yalu.zhang@iopsys.eu
 */

#ifndef __SERVICESVOICESERVICECAPABILITIES_H
#define __SERVICESVOICESERVICECAPABILITIES_H

#include <libbbf_api/dmcommon.h>

extern DMOBJ tServicesVoiceServiceCapabilitiesObj[];
extern DMLEAF tServicesVoiceServiceCapabilitiesParams[];
extern DMOBJ tServicesVoiceServiceCapabilitiesSIPObj[];
extern DMLEAF tServicesVoiceServiceCapabilitiesSIPClientParams[];
extern DMLEAF tServicesVoiceServiceCapabilitiesPOTSParams[];
extern DMLEAF tServicesVoiceServiceCapabilitiesCodecParams[];


#endif //__SERVICESVOICESERVICECAPABILITIES_H

