/*
 * Copyright (C) 2021 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	  Author Amin Ben Ramdhane <amin.benramdhane@pivasoftware.com>
 *
 */

#ifndef __VENDOR_EXTENSION_H
#define __VENDOR_EXTENSION_H

#include <libbbf_api/dmcommon.h>

extern DM_MAP_VENDOR tVendorExtension[];
extern DM_MAP_VENDOR tVendorExtensionOverwrite[];
extern DM_MAP_VENDOR_EXCLUDE tVendorExtensionExclude[];

#endif //__VENDOR_EXTENSION_H
