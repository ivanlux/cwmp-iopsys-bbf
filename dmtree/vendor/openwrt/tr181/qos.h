/*
 * Copyright (C) 2021 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	Author: Omar Kallel <omar.kallel@pivasoftware.com>
 *	Author: Amin Ben Ramdhane <amin.benramdhane@pivasoftware.com>
 */

#ifndef __OPENWRT_QOS_H
#define __OPENWRT_QOS_H

#include <libbbf_api/dmcommon.h>

extern DMOBJ tOPENWRT_QoSObj[];
extern DMLEAF tOPENWRT_QoSParams[];
extern DMLEAF tOPENWRT_QoSClassificationParams[];
extern DMLEAF tOPENWRT_QoSQueueStatsParams[];

#endif //__OPENWRT_QOS_H

