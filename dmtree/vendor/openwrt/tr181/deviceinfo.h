#ifndef __OPENWRT_DEVICEINFO_H
#define __OPENWRT_DEVICEINFO_H

#include <libbbf_api/dmcommon.h>

extern DMOBJ tOPENWRT_DeviceInfoObj[];
extern DMLEAF tOPENWRT_DeviceInfoParams[];
extern DMLEAF tOPENWRT_DeviceInfoFirmwareImageParams[];

#endif //__OPENWRT_DEVICEINFO_H
