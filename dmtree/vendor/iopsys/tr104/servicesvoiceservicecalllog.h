/*
 * Copyright (C) 2021 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *	Author Amin Ben Ramdhane <amin.benramdhane@pivasoftware.com>
 *	Author Grzegorz Sluja <grzegorz.sluja@iopsys.eu>
 */

#ifndef __IOPSYS_VOICESERVICECALLLOG_H
#define __IOPSYS_VOICESERVICECALLLOG_H

#include <libbbf_api/dmcommon.h>

extern DMLEAF tIOPSYS_VoiceServiceCallLogParams[];
extern DMLEAF tIOPSYS_VoiceServiceCallLogSessionSourceRTPParams[];
extern DMLEAF tIOPSYS_VoiceServiceCallLogSessionDestinationRTPParams[];

#endif //__IOPSYS_VOICESERVICECALLLOG_H

