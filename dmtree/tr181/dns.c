/*
 * Copyright (C) 2019 iopsys Software Solutions AB
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 *		Author: Amin Ben Ramdhane <amin.benramdhane@pivasoftware.com>
 */

#include "dmentry.h"
#include "dmdiagnostics.h"
#include "dmbbfcommon.h"
#include "dns.h"

/*************************************************************
* COMMON FUNCTIONS
**************************************************************/
static unsigned char is_dns_server_in_dmmap(char *chk_ip, char *chk_interface)
{
	struct uci_section *s = NULL;
	char *ip, *interface;

	uci_path_foreach_sections(bbfdm, "dmmap_dns", "dns_server", s) {
		dmuci_get_value_by_section_string(s, "ip", &ip);
		dmuci_get_value_by_section_string(s, "interface", &interface);
		if (strcmp(interface, chk_interface) == 0 && strcmp(ip, chk_ip) == 0) {
			return 1;
		}
	}
	return 0;
}

static int dmmap_synchronizeDNSClientRelayServer(struct dmctx *dmctx, DMNODE *parent_node, void *prev_data, char *prev_instance)
{
	json_object *jobj = NULL, *arrobj = NULL;
	struct uci_list *v;
	struct uci_element *e = NULL;
	struct uci_section *s = NULL, *sdns = NULL, *stmp = NULL, *ss;
	char *ipdns = NULL, *str, *vip = NULL, *viface;
	int j = 0;

	uci_path_foreach_sections_safe(bbfdm, "dmmap_dns", "dns_server", stmp, s) {
		dmuci_get_value_by_section_string(s, "ip", &vip);
		dmuci_get_value_by_section_string(s, "interface", &viface);
		int found = 0;
		uci_foreach_sections("network", "interface", ss) {
			if (strcmp(section_name(ss), viface) != 0)
				continue;
			dmuci_get_value_by_section_list(ss, "dns", &v);
			if (v != NULL) {
				uci_foreach_element(v, e) {
					if (strcmp(e->name, vip) == 0) {
						found = 1;
						break;
					}
				}
			}
			if (found)
				break;
			dmubus_call("network.interface", "status", UBUS_ARGS{{"interface", section_name(ss), String}}, 1, &jobj);
			if (!jobj) break;
			dmjson_foreach_value_in_array(jobj, arrobj, ipdns, j, 1, "dns-server") {
				if (strcmp(ipdns, vip) == 0) {
					found = 1;
					break;
				}
			}
			if (found)
				break;
		}
		if (!found)
			dmuci_delete_by_section(s, NULL, NULL);
	}

	uci_foreach_sections("network", "interface", s) {
		dmuci_get_value_by_section_list(s, "dns", &v);
		if (v != NULL) {
			uci_foreach_element(v, e) {
				if (is_dns_server_in_dmmap(e->name, section_name(s)))
					continue;
				dmuci_add_section_bbfdm("dmmap_dns", "dns_server", &sdns);
				dmuci_set_value_by_section(sdns, "ip", e->name);
				dmuci_set_value_by_section(sdns, "interface", section_name(s));
				dmuci_set_value_by_section(sdns, "enable", "1");
			}
		}
		dmuci_get_value_by_section_string(s, "peerdns", &str);
		if (str[0] == '0')
			continue;
		dmubus_call("network.interface", "status", UBUS_ARGS{{"interface", section_name(s), String}}, 1, &jobj);
		if (!jobj) break;
		dmjson_foreach_value_in_array(jobj, arrobj, ipdns, j, 1, "dns-server") {
			if (ipdns[0] == '\0' || is_dns_server_in_dmmap(ipdns, section_name(s)))
				continue;
			dmuci_add_section_bbfdm("dmmap_dns", "dns_server", &sdns);
			dmuci_set_value_by_section(sdns, "ip", ipdns);
			dmuci_set_value_by_section(sdns, "interface", section_name(s));
			dmuci_set_value_by_section(sdns, "enable", "1");
			dmuci_set_value_by_section(sdns, "peerdns", "1");
		}
	}
	return 0;
}

/*************************************************************
* ENTRY METHOD
**************************************************************/
static int browseDNSServerInst(struct dmctx *dmctx, DMNODE *parent_node, void *prev_data, char *prev_instance)
{
	struct uci_section *s = NULL;
	char *inst = NULL, *max_inst = NULL;

	dmmap_synchronizeDNSClientRelayServer(dmctx, NULL, NULL, NULL);
	uci_path_foreach_sections(bbfdm, "dmmap_dns", "dns_server", s) {

		inst = handle_update_instance(1, dmctx, &max_inst, update_instance_alias, 3,
			   s, "dns_server_instance", "dns_server_alias");

		if (DM_LINK_INST_OBJ(dmctx, parent_node, (void *)s, inst) == DM_STOP)
			break;
	}
	return 0;
}

static int browseResultInst(struct dmctx *dmctx, DMNODE *parent_node, void *prev_data, char *prev_instance)
{
	struct uci_section *s = NULL;
	char *inst = NULL, *max_inst = NULL;

	uci_path_foreach_sections(bbfdm, DMMAP_DIAGNOSTIGS, "NSLookupResult", s) {

		inst = handle_update_instance(1, dmctx, &max_inst, update_instance_alias, 3,
			   s, "nslookup_res_instance", "nslookup_res_alias");

		if (DM_LINK_INST_OBJ(dmctx, parent_node, (void *)s, inst) == DM_STOP)
			break;
	}
	return 0;
}

/*************************************************************
* ADD & DEL OBJ
**************************************************************/
static int add_dns_server(char *refparam, struct dmctx *ctx, void *data, char **instance)
{
	struct uci_section *s = NULL;

	char *inst = get_last_instance_bbfdm("dmmap_dns", "dns_server", "dns_server_instance");
	dmuci_add_list_value("network", "lan", "dns", "0.0.0.0");

	dmuci_add_section_bbfdm("dmmap_dns", "dns_server", &s);
	dmuci_set_value_by_section(s, "ip", "0.0.0.0");
	dmuci_set_value_by_section(s, "interface", "lan");
	dmuci_set_value_by_section(s, "enable", "1");

	*instance = update_instance(inst, 2, s, "dns_server_instance");
	return 0;
}

static int delete_dns_server(char *refparam, struct dmctx *ctx, void *data, char *instance, unsigned char del_action)
{
	struct uci_section *s = NULL, *ss = NULL, *stmp = NULL;
	char *interface, *ip, *str;
	struct uci_list *v;
	struct uci_element *e = NULL, *tmp = NULL;

	switch (del_action) {
		case DEL_INST:
			dmuci_get_value_by_section_string((struct uci_section *)data, "peerdns", &str);
			if (str[0] == '1')
				return 0;
			dmuci_get_value_by_section_string((struct uci_section *)data, "interface", &interface);
			dmuci_get_value_by_section_string((struct uci_section *)data, "ip", &ip);
			dmuci_del_list_value("network", interface, "dns", ip);
			dmuci_delete_by_section((struct uci_section *)data, NULL, NULL);
			break;
		case DEL_ALL:
			uci_foreach_sections("network", "interface", s) {
				dmuci_get_value_by_section_string(s, "peerdns", &str);
				if (str[0] == '1')
					continue;
				dmuci_get_value_by_section_list(s, "dns", &v);
				if (v != NULL) {
					uci_foreach_element_safe(v, e, tmp) {
						uci_path_foreach_option_eq_safe(bbfdm, "dmmap_dns", "dns_server", "ip", tmp->name, stmp, ss) {
							dmuci_delete_by_section(ss, NULL, NULL);
						}
						dmuci_del_list_value_by_section(s, "dns", tmp->name);
					}
				}
			}
			break;
	}
	return 0;
}

/*************************************************************
* GET & SET PARAM
**************************************************************/
static int get_dns_supported_record_types(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = "A,AAAA,PTR";
	return 0;
}

static int get_client_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = "1";
	return 0;
}

static int get_client_status(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = "Enabled";
	return 0;
}

static int get_client_server_number_of_entries(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	struct uci_section *s = NULL;
	int cnt = 0;

	dmmap_synchronizeDNSClientRelayServer(ctx, NULL, NULL, NULL);
	uci_path_foreach_sections(bbfdm, "dmmap_dns", "dns_server", s) {
		cnt++;
	}
	dmasprintf(value, "%d", cnt);
	return 0;
}

static int get_server_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
    *value = dmuci_get_value_by_section_fallback_def((struct uci_section *)data, "enable", "1");
    return 0;
}

static int get_server_status(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	char *v;

	dmuci_get_value_by_section_string((struct uci_section *)data, "enable", &v);
	*value = (*v == '1') ? "Enabled" : "Disabled";
	return 0;
}

static int get_server_alias(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "dns_server_alias", value);
	if ((*value)[0] == '\0')
		dmasprintf(value, "cpe-%s", instance);
	return 0;
}

static int get_server_dns_server(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "ip", value);
	return 0;
}

static int get_dns_interface(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	char *linker;

	dmuci_get_value_by_section_string((struct uci_section *)data, "interface", &linker);
	adm_entry_get_linker_param(ctx, "Device.IP.Interface.", linker, value);
	if (*value == NULL)
		*value = "";
	return 0;
}

static int get_dns_type(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	char *v;
	*value = "Static";
	dmuci_get_value_by_section_string((struct uci_section *)data, "peerdns", &v);
	if (*v == '1') {
		dmuci_get_value_by_section_string((struct uci_section *)data, "ip", &v);
		if (strchr(v, ':') == NULL)
			*value = "DHCPv4";
		else
			*value = "DHCPv6";
	}
	return 0;
}

static int get_relay_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	char *path = "/etc/rc.d/*dnsmasq";
	if (check_file(path))
		*value = "1";
	else
		*value = "0";
	return 0;
}

static int get_relay_status(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = (check_file("/etc/rc.d/*dnsmasq")) ? "Enabled" : "Disabled";
	return 0;
}

static int get_relay_forward_number_of_entries(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	struct uci_section *s = NULL;
	int cnt = 0;

	dmmap_synchronizeDNSClientRelayServer(ctx, NULL, NULL, NULL);
	uci_path_foreach_sections(bbfdm, "dmmap_dns", "dns_server", s) {
		cnt++;
	}
	dmasprintf(value, "%d", cnt);
	return 0;
}

static int get_forwarding_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = dmuci_get_value_by_section_fallback_def((struct uci_section *)data, "enable", "1");
    return 0;
}

static int get_forwarding_status(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	char *v;

	dmuci_get_value_by_section_string((struct uci_section *)data, "enable", &v);
	*value = (*v == '1') ? "Enabled" : "Disabled";
	return 0;
}

static int get_forwarding_alias(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "dns_server_alias", value);
	if ((*value)[0] == '\0')
		dmasprintf(value, "cpe-%s", instance);
	return 0;
}

static int get_forwarding_dns_server(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "ip", value);
	return 0;
}

static int get_nslookupdiagnostics_diagnostics_state(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = get_diagnostics_option_fallback_def("nslookup", "DiagnosticState", "None");
	return 0;
}

static int get_nslookupdiagnostics_interface(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	char *linker = get_diagnostics_option("nslookup", "interface");
	adm_entry_get_linker_param(ctx, "Device.IP.Interface.", linker, value);
	if (*value == NULL)
		*value = "";
	return 0;
}

static int get_nslookupdiagnostics_host_name(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = get_diagnostics_option("nslookup", "HostName");
	return 0;
}

static int get_nslookupdiagnostics_d_n_s_server(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = get_diagnostics_option("nslookup", "DNSServer");
	return 0;
}

static int get_nslookupdiagnostics_timeout(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = get_diagnostics_option_fallback_def("nslookup", "Timeout", "5000");
	return 0;
}

static int get_nslookupdiagnostics_number_of_repetitions(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = get_diagnostics_option_fallback_def("nslookup", "NumberOfRepetitions", "1");
	return 0;
}

static int get_nslookupdiagnostics_success_count(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = get_diagnostics_option_fallback_def("nslookup", "SuccessCount", "0");
	return 0;
}

static int get_nslookupdiagnostics_result_number_of_entries(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	struct uci_section *s = NULL;
	int cnt = 0;

	uci_path_foreach_sections(bbfdm, DMMAP_DIAGNOSTIGS, "NSLookupResult", s) {
		cnt++;
	}
	dmasprintf(value, "%d", cnt); // MEM WILL BE FREED IN DMMEMCLEAN
	return 0;
}

static int get_result_status(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = dmuci_get_value_by_section_fallback_def((struct uci_section *)data, "Status", "Error_Other");
	return 0;
}

static int get_result_answer_type(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "AnswerType", value);
	return 0;
}

static int get_result_host_name_returned(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "HostNameReturned", value);
	return 0;
}

static int get_result_i_p_addresses(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "IPAddresses", value);
	return 0;
}

static int get_result_d_n_s_server_i_p(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	dmuci_get_value_by_section_string((struct uci_section *)data, "DNSServerIP", value);
	return 0;
}

static int get_result_response_time(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = dmuci_get_value_by_section_fallback_def((struct uci_section *)data, "ResponseTime", "0");
	return 0;
}

static int set_client_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_boolean(value))
				return FAULT_9007;
			break;
		case VALUESET:
			break;
	}
	return 0;
}

static int set_dns_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	char *str, *ip, *interface;
	bool b, ob;

	switch (action) {
		case VALUECHECK:
			if (dm_validate_boolean(value))
				return FAULT_9007;
			break;
		case VALUESET:
			dmuci_get_value_by_section_string((struct uci_section *)data, "enable", &str);
			string_to_bool(value, &b);
			string_to_bool(str, &ob);
			if (ob == b)
				return 0;
			dmuci_get_value_by_section_string((struct uci_section *)data, "peerdns", &str);
			if (str[0] == '1')
				return 0;
			dmuci_set_value_by_section((struct uci_section *)data, "enable", b ? "1" : "0");
			dmuci_get_value_by_section_string((struct uci_section *)data, "interface", &interface);
			dmuci_get_value_by_section_string((struct uci_section *)data, "ip", &ip);
			if (b == 1)
				dmuci_add_list_value("network", interface, "dns", ip);
			else
				dmuci_del_list_value("network", interface, "dns", ip);
			break;
	}
	return 0;
}

static int set_server_alias(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 64, NULL, NULL))
				return FAULT_9007;
			break;
		case VALUESET:
			dmuci_set_value_by_section((struct uci_section *)data, "dns_server_alias", value);
			break;
	}
	return 0;
}

static int set_dns_server(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	char *str, *oip, *interface;
	struct uci_list *v;
	struct uci_element *e = NULL;
	int count = 0;
	char *dns[32] = {0};

	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 45, NULL, IPAddress))
				return FAULT_9007;
			break;
		case VALUESET:
			dmuci_get_value_by_section_string((struct uci_section *)data, "ip", &oip);
			if (strcmp(oip, value) == 0)
				return 0;
			dmuci_get_value_by_section_string((struct uci_section *)data, "peerdns", &str);
			if (str[0] == '1')
				return 0;
			dmuci_get_value_by_section_string((struct uci_section *)data, "interface", &interface);
			dmuci_get_option_value_list("network", interface, "dns", &v);
			if (v) {
				uci_foreach_element(v, e) {
					if (strcmp(e->name, oip)==0)
						dns[count] = dmstrdup(value);
					else
						dns[count] = dmstrdup(e->name);
					count++;
				}
			}
			dmuci_delete("network", interface, "dns", NULL);
			dmuci_get_value_by_section_string((struct uci_section *)data, "enable", &str);
			if (str[0] == '1') {
				int i = 0;

				for (i = 0; i < count; i++) {
					dmuci_add_list_value("network", interface, "dns", dns[i] ? dns[i] : "");
					dmfree(dns[i]);
				}
			}
			dmuci_set_value_by_section((struct uci_section *)data, "ip", value);
			break;
	}
	return 0;
}

static int set_dns_interface(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	char *str, *interface, *ip, *linker = NULL;

	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 256, NULL, NULL))
				return FAULT_9007;
			break;
		case VALUESET:
			adm_entry_get_linker_value(ctx, value, &linker);
			if (linker == NULL || linker[0] == '\0')
				return 0;

			dmuci_get_value_by_section_string((struct uci_section *)data, "interface", &interface);
			if (strcmp(interface, linker) == 0)
				return 0;
			dmuci_get_value_by_section_string((struct uci_section *)data, "peerdns", &str);
			if (str[0] == '1')
				return 0;
			dmuci_get_value_by_section_string((struct uci_section *)data, "ip", &ip);
			dmuci_del_list_value("network", interface, "dns", ip);
			dmuci_get_value_by_section_string((struct uci_section *)data, "enable", &str);
			if (str[0] == '1')
				dmuci_add_list_value("network", linker, "dns", ip);
			dmuci_set_value_by_section((struct uci_section *)data, "interface", interface);
			break;
	}
	return 0;
}

static int set_relay_enable(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	bool b;

	switch (action) {
		case VALUECHECK:
			if (dm_validate_boolean(value))
				return FAULT_9007;
			break;
		case VALUESET:
			string_to_bool(value, &b);
			dmcmd("/etc/init.d/dnsmasq", 1, b ? "enable" : "disable");
			break;
	}
	return 0;
}

static int set_forwarding_alias(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 64, NULL, NULL))
				return FAULT_9007;
			break;
		case VALUESET:
			dmuci_set_value_by_section((struct uci_section *)data, "dns_server_alias", value);
			break;
	}
	return 0;
}

static int set_nslookupdiagnostics_diagnostics_state(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, -1, DiagnosticsState, NULL))
				return FAULT_9007;
			return 0;
		case VALUESET:
			if (strcmp(value, "Requested") == 0) {
				NSLOOKUP_STOP
				set_diagnostics_option("nslookup", "DiagnosticState", value);
				bbf_set_end_session_flag(ctx, BBF_END_SESSION_NSLOOKUP_DIAGNOSTIC);
			}
			return 0;
	}
	return 0;
}

static int set_nslookupdiagnostics_interface(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 256, NULL, NULL))
				return FAULT_9007;
			return 0;
		case VALUESET:
			NSLOOKUP_STOP
			set_diagnostics_interface_option(ctx, "nslookup", value);
			return 0;
	}
	return 0;
}

static int set_nslookupdiagnostics_host_name(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 256, NULL, NULL))
				return FAULT_9007;
			return 0;
		case VALUESET:
			NSLOOKUP_STOP
			set_diagnostics_option("nslookup", "HostName", value);
			return 0;
	}
	return 0;
}

static int set_nslookupdiagnostics_d_n_s_server(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_string(value, -1, 256, NULL, NULL))
				return FAULT_9007;
			return 0;
		case VALUESET:
			NSLOOKUP_STOP
			set_diagnostics_option("nslookup", "DNSServer", value);
			return 0;
	}
	return 0;
}

static int set_nslookupdiagnostics_timeout(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_unsignedInt(value, RANGE_ARGS{{NULL,NULL}}, 1))
				return FAULT_9007;
			return 0;
		case VALUESET:
			NSLOOKUP_STOP
			set_diagnostics_option("nslookup", "Timeout", value);
			return 0;
	}
	return 0;
}

static int set_nslookupdiagnostics_number_of_repetitions(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	switch (action) {
		case VALUECHECK:
			if (dm_validate_unsignedInt(value, RANGE_ARGS{{NULL,NULL}}, 1))
				return FAULT_9007;
			return 0;
		case VALUESET:
			NSLOOKUP_STOP
			set_diagnostics_option("nslookup", "NumberOfRepetitions", value);
			return 0;
	}
	return 0;
}

/*************************************************************
 * OPERATE COMMANDS
 *************************************************************/
static operation_args dns_diagnostics_nslookup_args = {
	.in = (const char *[]) {
		"HostName",
		"Interface",
		"DNSServer",
		"Timeout",
		"NumberOfRepetitions",
		NULL
	},
	.out = (const char *[]) {
		"Status",
		"AnswerType",
		"HostNameReturned",
		"IPAddresses",
		"DNSServerIP",
		"ResponseTime",
		NULL
	}
};

static int get_operate_args_DNSDiagnostics_NSLookupDiagnostics(char *refparam, struct dmctx *ctx, void *data, char *instance, char **value)
{
	*value = (char *)&dns_diagnostics_nslookup_args;
	return 0;
}

static int operate_DNSDiagnostics_NSLookupDiagnostics(char *refparam, struct dmctx *ctx, void *data, char *instance, char *value, int action)
{
	struct uci_section *s = NULL;
	char *nslookup_status[2] = {0};
	char *nslookup_answer_type[2] = {0};
	char *nslookup_hostname_returned[2] = {0};
	char *nslookup_ip_addresses[2] = {0};
	char *nslookup_dns_server_ip[2] = {0};
	char *nslookup_response_time[2] = {0};
	int i = 1;

	init_diagnostics_operation("nslookup", NSLOOKUP_PATH);

	char *hostname = dmjson_get_value((json_object *)value, 1, "HostName");
	if (hostname[0] == '\0')
		return CMD_INVALID_ARGUMENTS;
	char *interface = dmjson_get_value((json_object *)value, 1, "Interface");
	char *dnsserver = dmjson_get_value((json_object *)value, 1, "DNSServer");
	char *timeout = dmjson_get_value((json_object *)value, 1, "Timeout");
	char *nbofrepetition = dmjson_get_value((json_object *)value, 1, "NumberOfRepetitions");

	set_diagnostics_option("nslookup", "HostName", hostname);
	set_diagnostics_interface_option(ctx, "nslookup", interface);
	set_diagnostics_option("nslookup", "DNSServer", dnsserver);
	set_diagnostics_option("nslookup", "Timeout", timeout);
	set_diagnostics_option("nslookup", "NumberOfRepetitions", nbofrepetition);

	// Commit and Free uci_ctx_bbfdm
	commit_and_free_uci_ctx_bbfdm(DMMAP_DIAGNOSTIGS);

	dmcmd("/bin/sh", 2, NSLOOKUP_PATH, "run");

	// Allocate uci_ctx_bbfdm
	dmuci_init_bbfdm();

	char *success_count = get_diagnostics_option("nslookup", "SuccessCount");
	add_list_parameter(ctx, dmstrdup("SuccessCount"), success_count, DMT_TYPE[DMT_UNINT], NULL);

	uci_path_foreach_sections(bbfdm, DMMAP_DIAGNOSTIGS, "NSLookupResult", s) {
		dmasprintf(&nslookup_status[0], "Result.%d.Status", i);
		dmasprintf(&nslookup_answer_type[0], "Result.%d.AnswerType", i);
		dmasprintf(&nslookup_hostname_returned[0], "Result.%d.HostNameReturned", i);
		dmasprintf(&nslookup_ip_addresses[0], "Result.%d.IPAddresses", i);
		dmasprintf(&nslookup_dns_server_ip[0], "Result.%d.DNSServerIP", i);
		dmasprintf(&nslookup_response_time[0], "Result.%d.ResponseTime", i);

		dmuci_get_value_by_section_string(s, "Status", &nslookup_status[1]);
		dmuci_get_value_by_section_string(s, "AnswerType", &nslookup_answer_type[1]);
		dmuci_get_value_by_section_string(s, "HostNameReturned", &nslookup_hostname_returned[1]);
		dmuci_get_value_by_section_string(s, "IPAddresses", &nslookup_ip_addresses[1]);
		dmuci_get_value_by_section_string(s, "DNSServerIP", &nslookup_dns_server_ip[1]);
		dmuci_get_value_by_section_string(s, "ResponseTime", &nslookup_response_time[1]);

		add_list_parameter(ctx, nslookup_status[0], nslookup_status[1], DMT_TYPE[DMT_STRING], NULL);
		add_list_parameter(ctx, nslookup_answer_type[0], nslookup_answer_type[1], DMT_TYPE[DMT_STRING], NULL);
		add_list_parameter(ctx, nslookup_hostname_returned[0], nslookup_hostname_returned[1], DMT_TYPE[DMT_STRING], NULL);
		add_list_parameter(ctx, nslookup_ip_addresses[0], nslookup_ip_addresses[1], DMT_TYPE[DMT_STRING], NULL);
		add_list_parameter(ctx, nslookup_dns_server_ip[0], nslookup_dns_server_ip[1], DMT_TYPE[DMT_STRING], NULL);
		add_list_parameter(ctx, nslookup_response_time[0], nslookup_response_time[1], DMT_TYPE[DMT_UNINT], NULL);
		i++;
	}

	return CMD_SUCCESS;
}

/**********************************************************************************************************************************
*                                            OBJ & LEAF DEFINITION
***********************************************************************************************************************************/
/* *** Device.DNS. *** */
DMOBJ tDNSObj[] = {
/* OBJ, permission, addobj, delobj, checkdep, browseinstobj, nextdynamicobj, dynamicleaf, nextobj, leaf, linker, bbfdm_type, uniqueKeys*/
{"Client", &DMREAD, NULL, NULL, NULL, NULL, NULL, NULL, tDNSClientObj, tDNSClientParams, NULL, BBFDM_BOTH},
{"Relay", &DMREAD, NULL, NULL, NULL, NULL, NULL, NULL, tDNSRelayObj, tDNSRelayParams, NULL, BBFDM_BOTH},
{"Diagnostics", &DMREAD, NULL, NULL, NULL, NULL, NULL, NULL, tDNSDiagnosticsObj, tDNSDiagnosticsParams, NULL, BBFDM_BOTH},
{0}
};

DMLEAF tDNSParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"SupportedRecordTypes", &DMREAD, DMT_STRING, get_dns_supported_record_types, NULL, BBFDM_BOTH},
{0}
};

/* *** Device.DNS.Client. *** */
DMOBJ tDNSClientObj[] = {
/* OBJ, permission, addobj, delobj, checkdep, browseinstobj, nextdynamicobj, dynamicleaf, nextobj, leaf, linker, bbfdm_type, uniqueKeys*/
{"Server", &DMWRITE, add_dns_server, delete_dns_server, NULL, browseDNSServerInst, NULL, NULL, NULL, tDNSClientServerParams, NULL, BBFDM_BOTH, LIST_KEY{"DNSServer", "Alias", NULL}},
{0}
};

DMLEAF tDNSClientParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"Enable", &DMWRITE, DMT_BOOL, get_client_enable, set_client_enable, BBFDM_BOTH},
{"Status", &DMREAD, DMT_STRING, get_client_status, NULL, BBFDM_BOTH},
{"ServerNumberOfEntries", &DMREAD, DMT_UNINT, get_client_server_number_of_entries, NULL, BBFDM_BOTH},
{0}
};

/* *** Device.DNS.Client.Server.{i}. *** */
DMLEAF tDNSClientServerParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"Enable", &DMWRITE, DMT_BOOL, get_server_enable, set_dns_enable, BBFDM_BOTH},
{"Status", &DMREAD, DMT_STRING, get_server_status, NULL, BBFDM_BOTH},
{"Alias", &DMWRITE, DMT_STRING, get_server_alias, set_server_alias, BBFDM_BOTH},
{"DNSServer", &DMWRITE, DMT_STRING, get_server_dns_server, set_dns_server, BBFDM_BOTH},
{"Interface", &DMWRITE, DMT_STRING, get_dns_interface, set_dns_interface, BBFDM_BOTH},
{"Type", &DMREAD, DMT_STRING, get_dns_type, NULL, BBFDM_BOTH},
{0}
};

/* *** Device.DNS.Relay. *** */
DMOBJ tDNSRelayObj[] = {
/* OBJ, permission, addobj, delobj, checkdep, browseinstobj, nextdynamicobj, dynamicleaf, nextobj, leaf, linker, bbfdm_type, uniqueKeys*/
{"Forwarding", &DMWRITE, add_dns_server, delete_dns_server, NULL, browseDNSServerInst, NULL, NULL, NULL, tDNSRelayForwardingParams, NULL, BBFDM_BOTH, LIST_KEY{"DNSServer", "Alias", NULL}},
{0}
};

DMLEAF tDNSRelayParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"Enable", &DMWRITE, DMT_BOOL, get_relay_enable, set_relay_enable, BBFDM_BOTH},
{"Status", &DMREAD, DMT_STRING, get_relay_status, NULL, BBFDM_BOTH},
{"ForwardNumberOfEntries", &DMREAD, DMT_UNINT, get_relay_forward_number_of_entries, NULL, BBFDM_BOTH},
{0}
};

/* *** Device.DNS.Relay.Forwarding.{i}. *** */
DMLEAF tDNSRelayForwardingParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"Enable", &DMWRITE, DMT_BOOL, get_forwarding_enable, set_dns_enable, BBFDM_BOTH},
{"Status", &DMREAD, DMT_STRING, get_forwarding_status, NULL, BBFDM_BOTH},
{"Alias", &DMWRITE, DMT_STRING, get_forwarding_alias, set_forwarding_alias, BBFDM_BOTH},
{"DNSServer", &DMWRITE, DMT_STRING, get_forwarding_dns_server, set_dns_server, BBFDM_BOTH},
{"Interface", &DMWRITE, DMT_STRING, get_dns_interface, set_dns_interface, BBFDM_BOTH},
{"Type", &DMREAD, DMT_STRING, get_dns_type, NULL, BBFDM_BOTH},
{0}
};

/* *** Device.DNS.Diagnostics. *** */
DMOBJ tDNSDiagnosticsObj[] = {
/* OBJ, permission, addobj, delobj, checkdep, browseinstobj, nextdynamicobj, dynamicleaf, nextobj, leaf, linker, bbfdm_type, uniqueKeys*/
{"NSLookupDiagnostics", &DMREAD, NULL, NULL, NULL, NULL, NULL, NULL, tDNSDiagnosticsNSLookupDiagnosticsObj, tDNSDiagnosticsNSLookupDiagnosticsParams, NULL, BBFDM_CWMP},
{0}
};

DMLEAF tDNSDiagnosticsParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"NSLookupDiagnostics()", &DMASYNC, DMT_COMMAND, get_operate_args_DNSDiagnostics_NSLookupDiagnostics, operate_DNSDiagnostics_NSLookupDiagnostics, BBFDM_USP},
{0}
};

/* *** Device.DNS.Diagnostics.NSLookupDiagnostics. *** */
DMOBJ tDNSDiagnosticsNSLookupDiagnosticsObj[] = {
/* OBJ, permission, addobj, delobj, checkdep, browseinstobj, nextdynamicobj, dynamicleaf, nextobj, leaf, linker, bbfdm_type, uniqueKeys*/
{"Result", &DMREAD, NULL, NULL, NULL, browseResultInst, NULL, NULL, NULL, tDNSDiagnosticsNSLookupDiagnosticsResultParams, NULL, BBFDM_CWMP},
{0}
};

DMLEAF tDNSDiagnosticsNSLookupDiagnosticsParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"DiagnosticsState", &DMWRITE, DMT_STRING, get_nslookupdiagnostics_diagnostics_state, set_nslookupdiagnostics_diagnostics_state, BBFDM_CWMP},
{"Interface", &DMWRITE, DMT_STRING, get_nslookupdiagnostics_interface, set_nslookupdiagnostics_interface, BBFDM_CWMP},
{"HostName", &DMWRITE, DMT_STRING, get_nslookupdiagnostics_host_name, set_nslookupdiagnostics_host_name, BBFDM_CWMP},
{"DNSServer", &DMWRITE, DMT_STRING, get_nslookupdiagnostics_d_n_s_server, set_nslookupdiagnostics_d_n_s_server, BBFDM_CWMP},
{"Timeout", &DMWRITE, DMT_UNINT, get_nslookupdiagnostics_timeout, set_nslookupdiagnostics_timeout, BBFDM_CWMP},
{"NumberOfRepetitions", &DMWRITE, DMT_UNINT, get_nslookupdiagnostics_number_of_repetitions, set_nslookupdiagnostics_number_of_repetitions, BBFDM_CWMP},
{"SuccessCount", &DMREAD, DMT_UNINT, get_nslookupdiagnostics_success_count, NULL, BBFDM_CWMP},
{"ResultNumberOfEntries", &DMREAD, DMT_UNINT, get_nslookupdiagnostics_result_number_of_entries, NULL, BBFDM_CWMP},
{0}
};

/* *** Device.DNS.Diagnostics.NSLookupDiagnostics.Result.{i}. *** */
DMLEAF tDNSDiagnosticsNSLookupDiagnosticsResultParams[] = {
/* PARAM, permission, type, getvalue, setvalue, bbfdm_type*/
{"Status", &DMREAD, DMT_STRING, get_result_status, NULL, BBFDM_CWMP},
{"AnswerType", &DMREAD, DMT_STRING, get_result_answer_type, NULL, BBFDM_CWMP},
{"HostNameReturned", &DMREAD, DMT_STRING, get_result_host_name_returned, NULL, BBFDM_CWMP},
{"IPAddresses", &DMREAD, DMT_STRING, get_result_i_p_addresses, NULL, BBFDM_CWMP},
{"DNSServerIP", &DMREAD, DMT_STRING, get_result_d_n_s_server_i_p, NULL, BBFDM_CWMP},
{"ResponseTime", &DMREAD, DMT_UNINT, get_result_response_time, NULL, BBFDM_CWMP},
{0}
};
