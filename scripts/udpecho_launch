#!/bin/sh
# Copyright (C) 2019 iopsys Software Solutions AB
# Author: AMIN Ben Ramdhane <amin.benramdhane@pivasoftware.com>

source /usr/share/bbfdm/bbf_uci_api

udpecho_launch() {
	local i proto device res ba stc times sc1 success_count failure_count min_time avg_time max_time avg_time_sum min max micros
	local host=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.Host`
	local port=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.port`
	local cnt=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.NumberOfRepetitions 1`
	local dsize=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.DataBlockSize 24`
	local dscp=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.DSCP 0`
	local interface=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.interface`
	local protocol=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.ProtocolVersion Any`
	local inter_time=`uci_get_bbf_dmmap dmmap_diagnostics.udpechodiag.InterTransmissionTime 1000`
	[ ! -z "$interface" ] && device=`ifstatus $interface | jsonfilter -e @.device` && device="-e $device" || device=""
	if [ "$protocol" == "IPv4" ]; then proto="-4"; elif [ "$protocol" == "IPv6" ]; then proto="-6"; else proto=""; fi
	local tos=$((dscp<<2))
	inter_time=$((inter_time/1000))
	[ "$inter_time" = "0" ] && inter_time="1"
	[ "$host" = "" ] && return
	[ "$port" = "" ] && return
	micros=1000
	success_count=0
	avg_time_sum=0
	min=9999999
	max=0
	i=0

	while [ $i -lt $cnt ]; do
		let i++
		res=$(nping $proto -c 1 --tos $tos --udp --dest-port $port --data-length $dsize $device $host 2>&1)
		ba=`echo "$res" | grep "RCVD"`
		[ -z "$ba" ] && { $UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.DiagnosticState=Error_CannotResolveHostName; $UCI_COMMIT_BBF_DMMAP; return; }
		stc=`echo "$res" | grep "RCVD" | grep "unreachable"`
		[ -n "$stc" ] && { $UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.DiagnosticState=Error_Other; $UCI_COMMIT_BBF_DMMAP; return; }
		times=`echo "$res" | grep "rtt"`
		[ -z "$times" ] && continue
		sc1=`echo "$res" | grep "Rcvd" | awk -F': ' '{print $3}' | awk -F'(' '{ print $1 }'`
		[ $sc1 != 0 ] && sc1=1 || sc1=0
		success_count=$((success_count+sc1))
		max_time=`echo $times | awk -F': ' '{ print $2 }' | awk -F'ms' '{ print $1 }'`
		min_time=`echo $times | awk -F': ' '{ print $3 }' | awk -F'ms' '{ print $1 }'`
		avg_time=`echo $times | awk -F': ' '{ print $4 }' | awk -F'ms' '{ print $1 }'`
		min_time=${min_time:-0}
		avg_time=${avg_time:-0}
		max_time=${max_time:-0}
		min_time=$(echo $min_time $micros | awk '{printf "%3.0f\n",$1*$2}')
		avg_time=$(echo $avg_time $micros | awk '{printf "%3.0f\n",$1*$2}')
		max_time=$(echo $max_time $micros | awk '{printf "%3.0f\n",$1*$2}')
		[ $min_time -lt $min ] && min=$min_time
		[ $max_time -gt $max ] && max=$max_time
		avg_time_sum=$((avg_time_sum+avg_time))
		sleep $inter_time
	done
	failure_count=$((cnt-success_count))
	[ $success_count -gt 0 ] && avg_time=$((avg_time_sum/success_count)) || avg_time=0
	min_time=$min
	max_time=$max
	$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.DiagnosticState=Complete
	$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.SuccessCount=$success_count
	$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.FailureCount=$failure_count
	$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.AverageResponseTime=$avg_time
	$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.MinimumResponseTime=$min_time
	$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.MaximumResponseTime=$max_time
	$UCI_COMMIT_BBF_DMMAP
}

udpecho_stop_diagnostic() {
	local pids=`ps | grep udpecho_launch | grep -v grep | awk '{print $1}'`
	if [ -n "$pids" ]; then
		kill -9 $pids &>/dev/null
		$UCI_SET_BBF_DMMAP dmmap_diagnostics.udpechodiag.DiagnosticState=None
		$UCI_COMMIT_BBF_DMMAP
	fi
}

if [ "$1" == "run" ]; then
	udpecho_launch
elif [ "$1" == "stop" ]; then
	udpecho_stop_diagnostic
else
	return
fi
